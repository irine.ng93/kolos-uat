<?php

namespace App\Helpers;

use App\User;

/**
 * Push Notifications
 */
class Notif
{

  public static function push($userId, $message, $eventId)
  {

    $apiKey = "AAAAS7PV8yU:APA91bGMYq7LJ1DpY9XkDUKaxjjWHk2S3SsW-Aw5Xk8FyleBwMgR6EdXAMq8wa6TtYIhtoSID3FehuA6MH0Foo_qBqes1SDaFzLs7jYTBgTpTXs29iryThtJ0eFrbrH4rCx4-qRbhXDw";
    $user = User::find($userId);
    $registrationIDs = array($user->gcm_id);
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
              'registration_ids'  => $registrationIDs,
              'data'              => array("message" => $message, "eventId" => $eventId),
            );
    $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
            );
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

    $result = curl_exec($ch);
    if(curl_errno($ch)){ echo 'Curl error: ' . curl_error($ch); }
    curl_close($ch);
    return response()->json($result);

  }

}



?>
