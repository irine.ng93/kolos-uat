<?php

namespace App\Console\Commands;

use DB;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AutoCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel order older than 15 mins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('orders')
            ->whereRaw('booking_time <= now() - interval 15 minute')
            ->where('status', 0)
            ->orderBy('created_at', 'ASC')
            ->update(['status' => 3]);
        $this->info('All declined orders are deleted successfully!');
    }
}
