<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestService extends Model
{
    protected $table = 'request_service';
    protected $fillable = ['user_id','service_name'];
}
