<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use App\Favorite;
use App\Alert;

use Auth;
use Validator;

/**
 * Follower Controllers
 */
class FollowerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex()
    {

        $followers = Favorite::join('users', 'users.id', '=', 'favorite_user.user_id')
                                ->where('favorite_user.merchant_id', Auth::user()->id)
                                ->where('favorite_user.user_id', '<>', Auth::user()->id)
                                ->get();
        return view('merchant.follower.view')->with('followers', $followers);
    }

    /**
     * Get view to push alert
     */
     public function getCreate()
     {
         return view('merchant.follower.create');
     }

     public function postStore(Request $request)
     {
         $valid = Validator::make($request->all(), [
             'message'      => 'required',
         ]);

         if ($valid->fails()) {
             return redirect()->back()
                ->withErrors($valid)
                ->withInput();
         } else {

             $followers = Favorite::join('users', 'users.id', '=', 'favorite_user.user_id')
                                     ->where('favorite_user.merchant_id', Auth::user()->id)
                                     ->where('favorite_user.user_id', '<>', Auth::user()->id)
                                     ->get();
             $message = $request->input('message');

             foreach ($followers as $follower) {
                 $alert = new Alert;
                 $alert->message = User::find(Auth::id())->merchant->company . ': ' . $message;
                 $alert->order_id = 0;
                 $alert->user_id = $follower->user_id;
                 $alert->icon = 2;
                 $alert->read = 0;
                 $alert->save();
             }

             $this->push($alert->user_id, $alert->message, 0);

             return redirect()->back()
                ->with('msg', 'Message has been pushed to followers');

         }
     }


     // push notifications
    public function push($user_id, $message, $eventId)
    {
        $apiKey = "AAAAS7PV8yU:APA91bGMYq7LJ1DpY9XkDUKaxjjWHk2S3SsW-Aw5Xk8FyleBwMgR6EdXAMq8wa6TtYIhtoSID3FehuA6MH0Foo_qBqes1SDaFzLs7jYTBgTpTXs29iryThtJ0eFrbrH4rCx4-qRbhXDw";
        $user = User::find($user_id);
        $registrationIDs = array($user->gcm_id);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
                'registration_ids'  => $registrationIDs,
                'data'              => array("message" => $message, "eventId" => $eventId),
                );
        $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
                );
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $result = curl_exec($ch);
        if(curl_errno($ch)){ echo 'Curl error: ' . curl_error($ch); }
        curl_close($ch);
        return response()->json($result);
    }
}


?>
