<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\UserMerchant;
use App\FormMerchant;
use App\Vemail;
use App\City;
use App\District;
use App\Service;
use App\Category;

use Validator;
use Session;
use Redirect;
use Auth;

class ProController extends Controller
{
    // step one -------------------
    public function getIndex()
    {

        //Session::flush();
        $categories = Category::all();
        return view('auth.pro.step1')->with('categories', $categories);

    }

    // step one process ***
    public function postNextone(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name'        => 'required|max:100',
            //'last_name'         => 'max:100',
            'email'             => 'required|max:100|email',
            //'username'        => 'required|min:4|max:100|alpha_num|unique:users,username',
            'password'          => 'required|confirmed|min:3',
            'phone'             => 'required|min:10|max:17',
            'company'           => 'required',
            'category'          => 'required'
        ]);

        if($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }
        else
        {
            // find the user
            $member = User::where('email', $request->input('email'))->first();

            if ($member == null) {
                $user = new User;
                $user->name         = $request->input('name');
                $user->username     = str_slug($request->input('name') . '_' . mt_rand(1111,9999));
                $user->email        = $request->input('email');
                $user->password     = bcrypt($request->input('password'));
                $user->phone        = str_replace('-', '', str_replace(' ','',$request->input('phone')));
                $user->status       = 1;
                $user->active       = 0;
                $user->phone_validation = mt_rand(11111, 99999);

                if ($user->save()) {
                    // create new merchant data
                    $merchant = new UserMerchant;
                    $merchant->user_id = $user->id;
                    $merchant->company = $request->input('company');
                    $merchant->category_id = $request->input('category');
                    $merchant->default_img = 'logo';
                    $merchant->active = 0;
                    $merchant->block = 0;
                    $merchant->featured = 0;
                    $merchant->save();

                    if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                        // Authentication passed...
                        return redirect('home')
                            ->with('msg', 'You have been succesfully registered, please fill your profile');
                    } else {
                        return redirect('login')
                            ->with('msg', 'You have been succesfully registered, please login');
                    }

                } else {
                    return redirect()->back()
                        ->with('err', 'Please try again!');
                }

            } else {

                $merchant_account = UserMerchant::where('user_id', $member->id)->first();

                if($merchant_account == null)
                {
                    $merchant = new UserMerchant;
                    $merchant->user_id = $member->id;
                    $merchant->company = $request->input('company');
                    $merchant->category_id = $request->input('category');
                    $merchant->default_img = 'logo';
                    $merchant->active = 0;
                    $merchant->block = 0;
                    $merchant->featured = 0;
                    $merchant->save();

                    if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                        // Authentication passed...
                        return redirect('home')
                            ->with('msg', 'You have been succesfully registered, please fill your profile');
                    } else {
                        return redirect('login')
                            ->with('msg', 'You have been succesfully registered, please login');
                    }
                }
                else
                {

                    if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                        // Authentication passed...
                        return redirect('home')
                            ->with('msg', 'Welcome back');
                    } else {
                        return redirect('login')
                            ->with('msg', 'You already registered, please login');
                    }
                }              

            }

        }
    }

    public function getSteplogin()
    {
        return view('auth.pro.stepLogin');
    }

    public function postSteploginprocess(Request $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            // Authentication passed...

            session(['user_id' => Auth::user()->id]);

            // create new merchant data
            $merchant = new UserMerchant;
            $merchant->user_id = Auth::user()->id;
            $merchant->save();

            session(['merchant_id' => $merchant->id]);

            session(['step' => '2']);
            return redirect('register/pro/two')
                ->with('msg', 'You have been succesfully registered, please continue the next step');
        }
    }

    // phone validation
    public function getPhonevalidation()
    {

        if (session()->has('step'))
        {
            $this->sendCode();
            //return view('auth.pro.step'. session('step'));
            return view('auth.pro.phone_validation');
        }
        else
        {
            return view('auth.pro.step1');
        }
        //return view('auth.pro.phone_validation');
    }

    // validate phone
    public function postValidatephone(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'code'  => 'required|numeric|digits:5',
        ]);

        if ($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid);
        }
        else
        {
            $user = User::find(session('user_id'));

            if ($user->phone_validation == $request->input('code'))
            {
                //$user->active = 1;

                if ($user->save())
                {
                    $merchant = UserMerchant::find(session('merchant_id'));
                    $merchant->active = 1;
                    $merchant->save();

//                    if (Auth::attempt(['email' => $user->email, 'password' => $user->password])) {
//                        // lets forget the session
//                        Session::flush();
//                        return redirect('home')->with('msg','Welcome New Merchant!');
//                    }

                    return redirect('home')
                        ->with('msg', 'Congratulations, your account has been activated. Lets login');
                }
                else
                {
                    return redirect()->back()
                    ->with('err', 'Oops, please try again.');
                }
            }
            else
            {
                return redirect()->back()
                    ->with('err', 'Wrong code.');
            }



        }
    }

    /*
    * Send SMS verification code
    */
    public function sendCode()
    {
        // Send a message to the phone number
        $user = User::find(session('user_id'));
        $api_user = 'APIZ0RGUW235W';
        $api_password = 'APIZ0RGUW235WCYXV3';
        $sms_from = 'KOLOS';
        $sms_to = $user->phone;
        $sms_msg = 'Your KOLOS verification code is '. $user->phone_validation.'.';
        //$this->sendCode($api_user,$api_password,$sms_from,$sms_to,$sms_msg); // callback

        //$user = User::find(session('user_id'));
        $query_string = "api.aspx?apiusername=".$api_user."&apipassword=".$api_password;
        $query_string .= "&senderid=".rawurlencode($sms_from)."&mobileno=".rawurlencode($sms_to);
        $query_string .= "&message=".rawurlencode(stripslashes($sms_msg)) . "&languagetype=1";
        $url = "http://gateway.onewaysms.co.id:10002/".$query_string;

        $arr = array($url);
        $fd = implode(',', $arr);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $fd);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl,CURLOPT_PROXYTYPE,CURLPROXY_HTTP);
        curl_setopt($curl,CURLOPT_PROXY,'http://proxy.shr.secureserver.net:3128');
        $result = curl_exec($curl);
        curl_close($curl);

        if ($fd) {
            if ($fd > 0) {
                Print("MT ID : " . $fd);
                $ok = "success";
            } else {
                print("Please refer to API on Error : " . $fd);
                $ok = "fail";
            }
        } else {
            // no contact with gateway
            $ok = "fail. No contact";
        }

        return $ok;

    }

    /*
    * GET District
    */
    /*
    public function postDistrict(Request $request)
    {
        $country = $request->input('city');

        $districts = District::where('city_id', $country)->orderBy('name', 'ASC')->get();

        if($country !== ''){
                echo "<option value=''>Select</option>";
            foreach($districts as $value){
                echo "<option value='$value->name'>". $value->name . "</option>";
            }
        } else {
            echo "<option>Select</option>";
        }

    }
    */
}
