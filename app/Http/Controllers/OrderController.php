<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Order;
use App\Review;
use App\Feedback;
use App\UserMerchant;
use App\User;
use App\Alert;

use Auth;
use DB;
use Mail;
use Notif;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex(Request $request)
    {

        if (Auth::user()->status == 2) { // for admin only

            if ($request->has('from') && $request->has('to'))
            {
                $orders = Order::whereBetween('created_at', array($request->input('from'), $request->input('to')))->paginate(20)->setPath('order');
            }
            else
            {

                $query = Order::query();

                if ($request->has('user_id')) {
                    $query->where('user_id', $request->get('user_id'));
                    $results = $query->paginate(20)->setPath('order?user_id='. $request->get('user_id'));
                } elseif ($request->has('merchant_id')) {
                    $query->where('merchant_id', $request->get('merchant_id'));
                    $results = $query->paginate(20)->setPath('order?merchant_id='. $request->get('merchant_id'));
                } else {
                    $query->orderBy('created_at','DESC');
                    $results = $query->paginate(20)->setPath('order');
                }

            }

            return view('admin.order.view')->with('orders', $results);

        } elseif(Auth::user()->status == 1){  // for merchants only

            if ($request->has('status')) {
                if ($request->get('status') == 'current') {

                    $orders = Order::where('merchant_id', '=', Auth::id())
                       ->where(function ($query) {
                           $query->where('status', '=', 0)
                                 ->orWhere('status', '=', 1);
                       })
                       ->orderBy('created_at', 'DESC')
                       ->paginate(20)->setPath('order?status=current');

                } elseif ($request->get('status') == 'past') {
                    $orders = Order::where('merchant_id', '=', Auth::id())
                       ->where(function ($query) {
                           $query->where('status', '=', 2)
                                 ->orWhere('status', '=', 3);
                       })
                       ->orderBy('created_at', 'DESC')
                       ->paginate(20)->setPath('order?status=past');
                } else {
                    $orders = Order::where('merchant_id', Auth::id())
                        ->orderBy('created_at', 'DESC')
                        ->paginate(20)->setPath('order');
                }

            } else {
                $orders = Order::where('merchant_id', Auth::id())
                    ->orderBy('created_at', 'DESC')
                    ->paginate(20)->setPath('order');
            }

            return view('admin.order.view')->with('orders', $orders);

        } else { // for everyone

            if ($request->input('status') == 'current') {
                $orders = Order::where('user_id', Auth::id())
                    ->where('status', 0)
                    ->orWhere('status', 1)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(20)->setPath('order');
            } elseif ($request->input('status') == 'completed') {
                $orders = Order::where('user_id', Auth::id())
                    ->where('status', 2)
                    ->orWhere('status', 3)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(20)->setPath('order');
            } else {
                $orders = Order::where('user_id', Auth::id())
                    ->orderBy('created_at', 'DESC')
                    ->paginate(20)->setPath('order');
            }

            return view('admin.order.view')->with('orders', $orders);
        }
    }

    // get detail of the user who make ann order
    public function getOrderdetail($id)
    {
        if (Auth::user()->status == 2)
        {
            $orders = Order::find($id);
            $review = Review::where('order_id', $id)->first();
                    $feedback = Feedback::where('order_id', $id)->first();
            return view('admin.order.user_detail')
                ->with('review', $review)
                ->with('feedback', $feedback)
                ->with('orders', $orders);
        }
        elseif (Auth::user()->status == 1)
        {
            $orders = Order::find($id);

            if ($orders != null) {

                if ($orders->merchant_id != Auth::id()) {
                    return redirect('home/order')
                        ->with('err','Order does not belongs to you!');
                } else {

                    $review = Review::where('order_id', $id)->first();
                    $feedback = Feedback::where('order_id', $id)->first();
                    $alert = Alert::where('order_id', $id)->first();
                    if ($alert !== null) {
                        $alert->read = 1;
                        $alert->save();
                    }

                    return view('admin.order.order_detail_public')
                        ->with('review', $review)
                        ->with('feedback', $feedback)
                        ->with('orders', $orders);

                }

            } else {
                return redirect('home/order')
                    ->with('err', 'Sorry, the order you looking for is doesn\'t exists');
            }

        } else {
            return redirect('home/order')
                ->with('err','Order does not belongs to you!');
        }

    }

    // order older than 15 minutes
    public function getOlderThanFifteenMinutes()
    {
      if (Auth::user()->status == 2)
      {
          $orders = Order::whereRaw('booking_time <= now() - interval 15 minute')
            ->where('status', 0)
            ->orderBy('created_at', 'ASC')
            ->paginate(20);
          return view('admin.order.view-time')
              ->with('orders', $orders);
      } else {

        return redirect('home/order')
          ->with('err', 'You dont have permission to access this page');

      }
    }

    public function getMostorderedmerchants()
    {
        //$merchants = Order::raw('COUNT(DISTINCT *) AS total')->groupBy('user_id')->get();
        $merchants = Order::select(DB::raw('count(*) as order_count, merchant_id'))
            ->groupBy('merchant_id')
            ->orderBy('order_count', 'DESC')
            ->get();
        return view('admin.order.mostorderedmerchants')->with('orders', $merchants);
    }

    public function getCustomerstatistics()
    {
        //$merchants = Order::raw('COUNT(DISTINCT *) AS total')->groupBy('user_id')->get();
        $merchants = Order::select(DB::raw('count(*) as order_count, user_id'))
            ->groupBy('user_id')
            ->orderBy('order_count', 'DESC')
            ->get();
        return view('admin.order.customers')->with('orders', $merchants);
    }

    public function postAccept(Request $request)
    {
        $order = Order::find($request->input('id'));

        if ($order->status == 0) {
            $order->status = 1;
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            if ($order->save()) {

                $message = 'Awesome news!! '. ($merchant->company == '' ? 'The Merchant' : $merchant->company) .' has accepted your request. Please review it in your order menu.';

                Notif::push($order->user_id, $message, 0);

                // save alert notification to merchant
                $alert = new Alert;
                $alert->user_id = $order->user_id;
                $alert->order_id = $order->id;
                $alert->message = 'Awesome news!! '. ($merchant->company == '' ? 'The Merchant' : $merchant->company) .' has accepted your request. Please review it in your order menu.';
                $alert->icon = '1';
                $alert->save();

                $user = User::find($order->user_id);
                $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
                Mail::send('emails.acceptedorder', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
                   $m->to($user->email, $user->name)->subject('Pesanan telah dikonfirmasi oleh '. $merchant->company);
                });

                return redirect()->back()
                    ->with('msg', 'Order has been confirmed');
            } else {
                return redirect()->back()
                    ->with('err', 'Order cannot be confirmed');
            }
        } else {
            return redirect()->back()
                    ->with('err', 'Order has been modified');
        }

    }

    public function postDecline(Request $request)
    {
        $order = Order::find($request->input('id'));

        if ($order->status == 0) {
            $order->status = 4;
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            if ($order->save()) {

                $message = 'Sorry, '. ($merchant->company == '' ? 'The Merchant' : $merchant->company) .' is currently unavailable. Please choose an other merchant.';

                Notif::push($order->user_id, $message, 1);

                // save alert notification to merchant
                $alert = new Alert;
                $alert->user_id = $order->user_id;
                $alert->order_id = $order->id;
                $alert->message = 'Sorry, '. ($merchant->company == '' ? 'The Merchant' : $merchant->company) .' is currently unavailable. Please choose an other merchant.';
                $alert->icon = '1';
                $alert->save();

                return redirect()->back()
                    ->with('msg', 'Order has been declined');
            } else {
                return redirect()->back()
                    ->with('err', 'Order cannot be declined');
            }
        } else {

            return redirect()->back()
                    ->with('err', 'Order has been modified');

        }

    }

    public function postComplete(Request $request)
    {
        $order = Order::find($request->input('id'));

        if ($order->status == 1) {
            $order = Order::find($request->input('id'));
            $order->status = 2;
            if ($order->save()) {

                $merchant = UserMerchant::find($order->merchant_id);
                $message = 'Your order has been complete.';

                Notif::push($order->user_id, $message, 0);

                return redirect()->back()
                    ->with('msg', 'Order has been completed');
            } else {
                return redirect()->back()
                    ->with('err', 'Order cannot be completed');
            }
        } else {
            return redirect()->back()
                ->with('err', 'Order has been modified');
        }

    }

    // delete
    public function postDelete(Request $request)
    {
      $id = $request->input('id');
        $user = Auth::user();

        $order = Order::where('id', $id)->first();

        if (Auth::user()->status != 2) {
            if ($order['merchant_id'] != $user->id) {
                return redirect()->back()
                    ->with('err', 'You don\'t have permission to delete this item');
            }
        }

        if ($order->delete()) {
            return redirect()->back()
            ->with('msg', 'Order has been deleted');
        } else {
            return redirect()->back()
            ->with('err', 'Order cannot be deleted');
        }
    }

    public function viewMerchantReviews(Request $request){
        $reviews = Review::orderBy('order_id', 'DESC')
            ->paginate(20)->setPath('order');
        return view('admin.order.view')->with('reviews', $reviews);
    }

}
