<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

class NotificationUccul extends Controller
{
    // push notifications
    public function push(Request $request)
    {
        //$apiKey = "AIzaSyDbNqntBTSqBJF-TFnVOHUueEiiTUzCO1Q";
        $apiKey = "AIzaSyCf4Hv8CquNu2ItIu1NlSnFwxM1HwFGoZk";
        //$registrationIDs = array("cZAe1oePDGA:APA91bE5ggQQvnq6Aprvz2hIXKaStv7rCme4Um0fX-hfO5tUnDEHFDTVk1XzfJYZcl7IxtCpRlik-W9pe6rz2R4NYqENdKLTbiIteljmorwVWNuKZpGJ_9dCKOoiy3I1uHhCHUwtFgY3");
        $registrationIDs = array("AAAA03guQb0:APA91bE79-RR6TrDq2nasNb_I4Gr1ksvUxW2kbs3Ar34VRZGsaR_ikqqLJVE7GmPhVjhx3R3WdXEwh3AIUp_DquF8N4wJX-mtGijaJAT6pPqWPgRlw-M6B-mjdeP7Qa6uHO4mgRKaRmr");
        $subject = 'Kolos Team';
        $message = 'Hello world!';
        //$message = array("subject"=>$subject, "message"=>$message);
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
                'registration_ids'  => $registrationIDs,
                'data'              => array("message"=>$message),
                );
        $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
                );

        //return response()->json($fields);

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
        curl_setopt( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $result = curl_exec($ch);
        if(curl_errno($ch)){ echo 'Curl error: ' . curl_error($ch); }
        curl_close($ch);
        return response()->json($result);
    }

}
