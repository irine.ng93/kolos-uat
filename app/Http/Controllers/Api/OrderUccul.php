<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\UserMerchant;
use App\Service;
use App\Order;
use App\OrderService;
use App\Review;
use App\Feedback;
use App\Alert;

use Auth;
use Mail;
use Validator;


class OrderUccul extends Controller
{

    public function createOrderNumber($merchant_id){
        $categoryID = UserMerchant::where('user_id', '=', $merchant_id)->first();
        $randNumber = mt_rand(10000, 99999);
        $orderNum = "O".substr(date("Y"),2,2).$categoryID->category_id."1".$randNumber;
        $orderHistory = Order::where('order_num', '=', $orderNum)->first();

        if($orderHistory != null){
            $orderNum = "O".$categoryID->category_id."1".$randNumber+1;
        }
    }

    // create order
    public function placeOrder(Request $request)
    {
        $consumer = Auth::user();
        $merchant_id = $request->input('merchant_id');
        $services = $request->input('services');
        $date = date_create($request->input('booking_time'));
        $location = $request->input('location');
        $location_detail = $request->input('location_detail');
        $lon = $request->input('lon');
        $lat = $request->input('lat');
        $note = $request->input('note');

        // create the order
        $order = new Order;
        $order->user_id = $consumer->id;
        $order->merchant_id = $merchant_id;
        $order->booking_time = $date;
        $order->location = $location;
        $order->location_detail = $location_detail;
        $order->lon = $lon;
        $order->lat = $lat;
        $order->note = $note;

        if ($order->save()) {
            for ($i=0; $i < count($services); $i++) { 
                $orderService = new OrderService();
                $orderService->order_id = $order->id;
                $orderService->service_id = $services[$i]['service_id'];
                $orderService->quantity = $services[$i]['quantity'];
                $orderService->save();
            }
        }

        $alert = new Alert;
        $alert->user_id = $merchant_id;
        $alert->order_id = $order->id;

        $alert->message = 'Great news!! You get an order from '. $consumer->name .'. Please review it in Order Pro menu.';
        $alert->message_id = 'Pesanan baru dari '. $consumer->name .' tanggal '. $order->booking_time->format('d F Y') . '. Silahkan lakukan konfirmasi.';
        $alert->icon = '1';
        $alert->save();

        $merchant = UserMerchant::where('user_id', $merchant_id)->firstOrFail();
        $alertm = new Alert;
        $alertm->user_id = $consumer->id;
        $alertm->order_id = $order->id;
        $alertm->message = 'Thank you!! Your order has been made. '. ($merchant['company'] == '' ? 'The Merchant' : $merchant['company']) .' will get back to you shortly.';
        $alertm->message_id = 'Terimakasih. Pesanan kamu akan segera diproses oleh '. ($merchant['company'] == '' ? 'Penjual' : $merchant['company']);
        $alertm->icon = '2';
        $alertm->save();

        // sending email to merchant
        $user = User::find($order->user_id);

        $this->push($merchant_id, 'Pesanan baru dari '. $user->name .'. Silahkan lakukan konfirmasi.', 1);
        $merchant_user = User::find($alert->user_id);
        Mail::send('emails.neworder', ['user' => $user, 'merchant'=>$merchant_user, 'order' => $order], function ($m) use ($user, $merchant_user, $order) {
           $m->to($merchant_user->email, $merchant_user->name)->subject('Pesanan baru dari '. $user->name .' tanggal '. $order->booking_time->format('d F Y'));
        });

        // sending email to user
        Mail::send('emails.madeorder', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
           $m->to($user->email, $user->name)->subject('Pesanan telah berhasil diteruskan ke '. $merchant->company .' tanggal '. $order->booking_time->format('d F Y'));
        });

        // sending email to kolos ppl
        Mail::send('emails.orderguys', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
           $m->to('order@kolos.co.id', $user->name)->subject('Pesanan baru untuk '. $merchant->company .' tanggal '. $order->booking_time->format('d F Y'));
        });

        return response()->json(array(
            'status' => 200,
        ));
    }


    public function create(Request $request)
    {

        $user_id = Auth::user()->id;
        $consumer = User::find($user_id);

        $merchant_id = $request->input('merchant_id');
        $services = json_decode(stripslashes($request->input('service_id')), true);
        $date = date_create($request->input('booking_time'));
        $categoryID = UserMerchant::where('user_id', '=', $merchant_id)->first();

        $order = new Order;
        $randNumber = mt_rand(10000, 99999);
        $orderNum = "O".substr(date("Y"),2,2).$categoryID->category_id."1".$randNumber;
        $orderHistory = Order::where('order_num', '=', $orderNum)->first();

        if($orderHistory != null){
            $orderNum = "O".$categoryID->category_id."1".$randNumber+1;
        }
        $order->order_num = $orderNum;
        $order->user_id = $user_id;
        $order->merchant_id = $merchant_id;
        $order->booking_time = date_format($date, 'Y-m-d H:i:s');
        $order->location = $request->input('location');
        $order->lon = $request->input('lon');
        $order->lat = $request->input('lat');
        $order->note = $request->input('note');

        if ($order->save()) {
            $responseStatus = 200;
            $responseData = 'test success';
            foreach ($services as $service) {
                $orderService = new OrderService();
                $orderService->order_id = $order->id;
                $orderService->service_id = $service['service_id'];
                $orderService->quantity = $service['quantity'];
                $orderService->save();
            }
            // send push notification to merchant
            $message = 'Great news!! You get an order from '. $consumer->name .'. Please review it in your order pro menu.';
            $this->push($merchant_id, $message, 1);

            // save alert notification to merchant
            $alert = new Alert;
            $alert->user_id = $merchant_id;
            $alert->order_id = $order->id;
            $alert->message = 'Great news!! You get an order from '. $consumer->name .'. Please review it in Order Pro menu.';
            $alert->message_id = 'Selamat!! Anda mendapat pesanan dari '. $consumer->name .'. Silakan melihat pesanan tersebut di menu Order Pro.';
            $alert->icon = '1';

            if ($alert->save()) {
                // save alert notification to user
                $seller = UserMerchant::where('user_id', $request->input('merchant_id'))->first();
                $alert = new Alert;
                $alert->user_id = $user_id;
                $alert->order_id = $order->id;
                $alert->message = 'Thank you!! Your order has been made. '. ($seller['company'] == '' ? 'The Merchant' : $seller['company']) .' will get back to you shortly.';
                $alert->message_id = 'Terima kasih!! Pesanan anda telah dibuat. '. ($seller['company'] == '' ? 'Merchant' : $seller['company']) .' akan segera menghubungi Anda.';
                $alert->icon = '2';
                $alert->save();

                //send email
                // $data = array('merchantname'=>$seller['company'],'customername'=>$consumer->name,'bookingtime'=>date_format($order->booking_time, 'd-M-Y H:i:s'),'address'=>$order->location);
                // Mail::queue('emails.neworder', $data, function($message) {
                //              $message->to('contact@kolos.co.id', 'KOLOS')
                //              ->subject('New Order Notification')
                //              ->from('contact@kolos.co.id','KOLOS');
                //             });

                $user = User::find($user_id);
                $merchant_user = User::find($request->input('merchant_id'));
                //send email to merchant
                Mail::send('emails.neworder', ['user' => $user, 'merchant'=>$merchant_user, 'order' => $order], function ($m) use ($user, $merchant_user, $order) {
                   $m->to($merchant_user->email, $merchant_user->name)->subject('Pesanan baru dari '. $user->name .' tanggal '. $order->booking_time->format('d F Y'));
                });

                // sending email to user
                Mail::send('emails.madeorder', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
                   $m->to($user->email, $user->name)->subject('Pesanan telah berhasil diteruskan ke '. $merchant->company .' tanggal '. $order->booking_time->format('d F Y'));
                });

                // sending email to kolos ppl
                Mail::send('emails.orderguys', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
                   $m->to('order@kolos.co.id', $user->name)->subject('Pesanan baru untuk '. $merchant->company .' tanggal '. $order->booking_time->format('d F Y'));
                });
            }
            else{
                $responseStatus = 0;
                $responseData = 'failed';
            }
        }

        return response()->json(array(
            'status' => $responseStatus,
            'data' => $responseData,
            'service' => $service,
        ));
    }

    /*
    * List of orders for customer users
    * -----------------------------------------
    */
    public function view(Request $request)
    {
    	$id = Auth::user()->id;

        // data past orders
        $orders = Order::where('user_id', '=', Auth::user()->id)
            ->where(function ($query2) {
                $query2->where('status', '=', 2)
                      ->orWhere('status', '=', 3)
                      ->orWhere('status', '=', 4);
            })
            ->orderBy('created_at', 'DESC')
            ->get();

        $i = 0;
        foreach ($orders as $key) :
            $orders[$i]['services'] = OrderService::select('services.id','services.name','services.price','services.description','order_services.quantity')
                ->leftJoin('services', 'services.id', '=', 'order_services.service_id')
                ->where('order_services.order_id', $orders[$i]->id)
                ->get();
            $merchantModel = UserMerchant::select('users.phone','user_merchant.company','user_merchant.logo')
                                        ->rightJoin('users','users.id','=','user_merchant.user_id')
                                        ->where('user_merchant.user_id', $orders[$i]->merchant_id)
                                        ->first();
            if($merchantModel != null){
                $orders[$i]['merchant_name'] = $merchantModel->company;
                $orders[$i]['merchant_image'] = $merchantModel->logo;
                $orders[$i]['merchant_phone'] = $merchantModel->phone;
            }
            else{
                $orders[$i]['merchant_name'] = '';
                $orders[$i]['merchant_image'] = '';
                $orders[$i]['merchant_phone'] = '';
            }
            
        $i++;
        endforeach;


        // data current orders
        $orders_current = Order:: where('user_id', '=', Auth::user()->id)
            ->where(function ($query) {
                $query->where('status', '=', 0)
                      ->orWhere('status', '=', 1);
            })
            ->orderBy('created_at', 'DESC')
            ->get();

        $j = 0;
        foreach ($orders_current as $key){
            $orders_current[$j]['services'] = OrderService::select('services.id','services.name','services.price','services.description','order_services.quantity')
                ->leftJoin('services', 'services.id', '=', 'order_services.service_id')
                ->where('order_services.order_id', $orders_current[$j]->id)
                ->get();

            $merchantModel = UserMerchant::select('users.phone','user_merchant.company','user_merchant.logo')
                                        ->rightJoin('users','users.id','=','user_merchant.user_id')
                                        ->where('user_merchant.user_id', $orders_current[$j]->merchant_id)
                                        ->first();
            if($merchantModel != null){
                $orders_current[$j]['merchant_name'] = $merchantModel->company;
                $orders_current[$j]['merchant_image'] = $merchantModel->logo;
                $orders_current[$j]['merchant_phone'] = $merchantModel->phone;
            }
            else{
                $orders_current[$j]['merchant_name'] = '';
                $orders_current[$j]['merchant_image'] = '';
                $orders_current[$j]['merchant_phone'] = '';
            }
            $j += 1;
        }

        return response()->json(array(
            'status' => 200,
            'error' => false,
            'data_past' => $orders,
            'data_current' => $orders_current,
        ));
    }

    // order view pro
    public function viewpro(Request $request)
    {
        $id = Auth::user()->id;

        if (Auth::user()->status != 1)
        {
            return response()->json([
                'error' => true,
                'message'   => 'Youre not a merchant!',
            ]);
        }

    	$user = User::find($id);

        $orders = Order::select('orders.*')
            ->leftjoin('order_services', 'order_services.order_id', '=', 'orders.id')
            ->where('orders.merchant_id', '=', $id)
            ->where(function ($query) {
                $query->where('orders.status', '=', 2)
                      ->orWhere('orders.status', '=', 3)
                      ->orWhere('orders.status', '=', 4);
            })
            ->orderBy('orders.created_at', 'DESC')
            ->get();

        $i = 0;
        foreach ($orders as $key) :
            $orders[$i]['services'] = OrderService::select('services.id','services.name','services.price', 'services.description','order_services.quantity')
                ->leftJoin('services', 'services.id', '=', 'order_services.service_id')
                ->where('order_services.order_id', $orders[$i]->id)
                ->get();
            $orders[$i]['merchant_name'] = UserMerchant::select('company')->where('user_id', $orders[$i]->merchant_id)->first()->company;
            $orders[$i]['customer_name'] = User::where('id', $orders[$i]->user_id)->first()->name;

            $orders[$i]['merchant_image'] = UserMerchant::select('logo')->where('user_id', $orders[$i]->merchant_id)->first()->logo;
            $orders[$i]['customer_image'] = User::where('id', $orders[$i]->user_id)->first()->avatar;

            $orders[$i]['user_phone'] = User::select('phone')->where('id', $orders[$i]->user_id)->first()->phone;
        $i++;
        endforeach;

        $orders_current = Order::select('orders.*')
            ->leftjoin('order_services', 'order_services.order_id', '=', 'orders.id')
            ->where('orders.merchant_id', '=', $id)
            ->where(function ($query) {
                $query->where('orders.status', '=', 0)
                      ->orWhere('orders.status', '=', 1);
            })
            ->orderBy('orders.created_at', 'DESC')
            ->get();

        $i = 0;
        foreach ($orders_current as $key) :
            $orders_current[$i]['services'] = OrderService::select('services.id','services.name','services.price','services.description','order_services.quantity')
                ->leftJoin('services', 'services.id', '=', 'order_services.service_id')
                ->where('order_services.order_id', $orders_current[$i]->id)
                ->get();
            $orders_current[$i]['merchant_name'] = UserMerchant::select('company')->where('user_id', $orders_current[$i]->merchant_id)->first()->company;
            $orders_current[$i]['customer_name'] = User::where('id', $orders_current[$i]->user_id)->first()->name;
            $orders_current[$i]['customer_image'] = User::where('id', $orders_current[$i]->user_id)->first()->avatar;
            $orders_current[$i]['merchant_image'] = UserMerchant::select('logo')->where('user_id', $orders_current[$i]->merchant_id)->first()->logo;
            $orders_current[$i]['user_phone'] = User::select('phone')->where('id', $orders_current[$i]->user_id)->first()->phone;
        $i++;
        endforeach;

        return response()->json(array(
            'status' => 200,
            'data_past' => $orders,
            'data_current' => $orders_current,
        ));

    }

    // get number order
    public function ordernum(Request $request)
    {
        $orders = Order::select('orders.*')
                ->where('orders.user_id', Auth::user()->id)
                ->leftjoin('order_services', 'order_services.order_id', '=', 'orders.id')
                ->where('orders.status', 0)
                ->orWhere('orders.status', 1)
                ->count();

        return response()->json([
            'status' => 200,
            'ordernum' => $orders,
        ]);
    }

    // get number order pro
    public function orderpronum(Request $request)
    {
        $orders = Order::select('orders.*')
                ->where('orders.merchant_id', Auth::user()->id)
                ->leftjoin('order_services', 'order_services.order_id', '=', 'orders.id')
                ->where('orders.status', 0)
                ->orWhere('orders.status', 1)
                ->count();

        return response()->json([
            'status' => 200,
            'ordernum' => $orders,
        ]);
    }

    // order detail
    public function detail(Request $request)
    {
        $id = $request->input('id');
        $order = Order::find($id);
        $review = Review::where('order_id', $id)->get();
        $feedback = Feedback::where('order_id', $id)->get();

        if ($order == null)
        {
            return response()->json(array(
                'status' => 201,
                'order' => '[]',
            ));
        }
        else
        {
            return response()->json([
                'status' => 200,
                'order' => $order,
                'review' => ($review == null ? '' : $review),
                'feedback' => ($feedback == null ? '' : $feedback),
            ]);
        }
    }

    public function detailNew(Request $request)
    {
        $id = $request->input('id');
        $order = Order::where('orders.id', $id)
            ->where('orders.user_id', Auth::id())
            ->leftjoin('user_merchant as um', 'um.user_id', '=', 'orders.merchant_id')
            ->leftjoin('users', 'users.id', '=', 'orders.merchant_id')
            ->select('orders.*', 'um.company as merchant_name', 'um.location_detail as merchant_location_detail', 'um.location as merchant_location', 'um.logo as merchant_logo', 'users.phone as merchant_phone')
            ->first();
        $review = Review::where('order_id', $id)->first();
        // $feedback = Feedback::where('order_id', $id)->get();

        if ($order === null)
        {
            return response()->json(array(
                'status' => 201,
                'order' => null,
                'review' => null,
            ));
        }
        else
        {
            // $order['merchant'] = UserMerchant::findOrFail($order->merchant_id);
            $order['services'] = OrderService::select('order_services.*','services.price', 'services.name', 'services.description')
                ->leftjoin('services', 'order_services.service_id', '=', 'services.id')
                ->where('order_services.order_id', $order->id)
                ->get();
            return response()->json([
                'status' => 200,
                'order' => $order,
                'review' => ($review == null ? '' : $review),
                // 'feedback' => ($feedback == null ? '' : $feedback),
            ]);
        }
    }

    // accept order
    public function accept(Request $request)
    {
        //$id = $request->input('id'); // merchant_id
        $order_id = $request->input('id');
        $order = Order::where('id', $order_id)->first();
        $order->status = 1;
        if ($order->save()) {

            // save alert notification
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            $alert = new Alert;
            $alert->user_id = $order->user_id;
            $alert->order_id = $order->id;
            $alert->message = 'Awesome news!! '. ($merchant['company'] == '' ? 'The Merchant' : $merchant['company']) .' has accepted your request. Please review it in your order menu.';
            $alert->message_id = 'Berita baik!! '. ($merchant['company'] == '' ? 'Merchant' : $merchant['company']) .' telah menerima pesanan Anda. Silakan mengecek di bagian menu Order.';
            $alert->icon = '1';
            $alert->save();

            $this->push($order->user_id, $alert->message, 0);

            // send email to user
            $user = User::find($order->user_id);
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            Mail::send('emails.acceptedorder', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
               $m->to($user->email, $user->name)->subject('Pesanan telah dikonfirmasi oleh '. $merchant->company);
            });

            return response()->json(array(
                'error' => false,
                'status' => 200,
                'data' => 'success',
            ));
        } else {
            return response()->json(array(
                'error' => true,
                'status' => 201,
                'data' => 'fail',
            ));
        }

    }

    // complete order
    public function complete(Request $request)
    {
        $order_id = $request->input('id');
        $order = Order::where('id', $order_id)->first();
        $order->status = 2;
        if ($order->save()) {
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            $alert = new Alert;
            $alert->user_id = $order->user_id;
            $alert->order_id = $order->id;
            $alert->message = 'Your order has been completed by '.$merchant['company'].', Please leave a review for '.$merchant['company'].'.';
            $alert->message_id = 'Pesanan Anda telah diselesaikan oleh '.$merchant['company'].', Silakan beri ulasan untuk '.$merchant['company'].'.';
            $alert->icon = '1';
            $alert->save();

            $this->push($order->user_id, $alert->message, 0);

            return response()->json(array(
                'error' => false,
                'status' => 200,
                'data' => 'success',
            ));
        } else {
            return response()->json(array(
                'error' => true,
                'status' => 201,
                'data' => 'fail',
            ));
        }
    }

    // decline
    public function decline(Request $request)
    {
        $order_id = $request->input('id');
        $order = Order::where('id', $order_id)->first();
        $order->status = 4;
        if ($order->save()) {

            // save alert notification
            //$merchant = UserMerchant::find($order->merchant_id);
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            $alert = new Alert;
            $alert->user_id = $order->user_id;
            $alert->order_id = $order->id;
            $alert->message = 'Sorry, '. ($merchant['company'] == '' ? 'The Merchant' : $merchant['company']) .' is currently unavailable. Please choose another merchant.';
            $alert->message_id = 'Maaf, '. ($merchant['company'] == '' ? 'Merchant' : $merchant['company']) .' sedang tidak tersedia saat ini. Silakan memilih jasa dari merchant lain.';
            $alert->icon = '3';
            $alert->save();

            $this->push($order->user_id, $alert->message, 0);

            // send email to user
            $user = User::find($order->user_id);
            Mail::send('emails.declinedorder', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) {
               $m->to($user->email, $user->name)->subject('Pesanan telah dikonfirmasi oleh '. $merchant->company);
            });

            return response()->json(array(
                'error' => false,
                'status' => 200,
                'data' => 'success',
            ));
        } else {
            return response()->json(array(
                'error' => true,
                'status' => 201,
                'data' => 'fail',
            ));
        }
    }

    // cancel order by customer
    public function cancel(Request $request)
    {
        $order_id = $request->input('id');
        $order = Order::where('id', $order_id)->first();
        $order->status = 3;
        if ($order->save()) {
            // save alert notification
            $user = User::find($order->user_id);
            $merchant = UserMerchant::where('user_id', $order->merchant_id)->first();
            $alert = new Alert;
            $alert->user_id = $order->merchant_id;
            $alert->order_id = $order->id;
            $alert->message = 'Sorry, '. $user->name .' has canceled his order. Please wait for another order.';
            $alert->message_id = 'Maaf, '. $user->name .' membatalkan pesanannya. Silakan menunggu pesanan selanjutnya.';
            $alert->icon = '3';
            $alert->save();

            $alert = new Alert;
            $alert->user_id = $order->user_id;
            $alert->order_id = $order->id;
            $alert->message = 'You have canceled an order from '.$merchant->company.'. We are sorry for the inconvenience.';
            $alert->message_id = 'Anda baru membatalkan pesanan dari '.$merchant->company.'. Mohon maaf atas ketidaknyamanan Anda.';
            $alert->icon = '3';
            $alert->save();

            // send email to merchant
//            $user = User::find($order->user_id);
//            Mail::send('emails.canceledorder', ['user' => $user], function ($m) use ($user) {
//                //$m->from('hello@app.com', 'Your Application');
//                $m->to($user->email, $user->name)->subject('Sorry, '. $user->name .' has canceled his order.');
//            });
            $merchant = User::find($order->merchant_id); 
            Mail::send('emails.canceledorder', ['user' => $user, 'merchant'=>$merchant, 'order' => $order], function ($m) use ($user, $merchant, $order) { 
               $m->to($merchant->email, $merchant->name)->subject('Pesanan telah dibatalkan oleh '. $user->name); 
            }); 

            $this->push($order->merchant_id, $alert->message, 1);

            return response()->json(array(
                'error' => false,
                'status' => 200,
                'data' => 'success',
            ));
        } else {
            return response()->json(array(
                'error' => true,
                'status' => 201,
                'data' => 'fail',
            ));
        }
    }

    // sms notifications
    public function sendSms($user_id)
    {
      $user = User::find($user_id);
      if ( $user->phone != '' ) {
        if ( $user->phone[0] == '0' ) {
          $phone = '+62' . substr($user->phone, 1);
        } else {
          $phone = $user->phone;
        }
      }
      $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
          [
            'api_key' =>  '3bb35f14',
            'api_secret' => 'dd6a0fc0b7651e7f',
            'to' => $phone,
            'from' => 'KOLOS',
            'text' => 'Halo '. $user->name .', kamu mendapatkan pesanan baru!. Silahkan buka aplikasi KOLOS untuk memproses pesanan.'
          ]
      );

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);

      return response()->json($response);
    }


    // push notifications
    public function push($user_id, $message, $eventId)
    {
        //$apiKey = "AAAAS7PV8yU:APA91bGMYq7LJ1DpY9XkDUKaxjjWHk2S3SsW-Aw5Xk8FyleBwMgR6EdXAMq8wa6TtYIhtoSID3FehuA6MH0Foo_qBqes1SDaFzLs7jYTBgTpTXs29iryThtJ0eFrbrH4rCx4-qRbhXDw";
        $apiKey = "AAAA03guQb0:APA91bE79-RR6TrDq2nasNb_I4Gr1ksvUxW2kbs3Ar34VRZGsaR_ikqqLJVE7GmPhVjhx3R3WdXEwh3AIUp_DquF8N4wJX-mtGijaJAT6pPqWPgRlw-M6B-mjdeP7Qa6uHO4mgRKaRmr";
        $user = User::find($user_id);
        $registrationIDs = array($user->gcm_id);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
                'registration_ids'  => $registrationIDs,
                'data'              => array("message" => $message, "eventId" => $eventId),
                );
        $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
                );
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $result = curl_exec($ch);
        if(curl_errno($ch)){ echo 'Curl error: ' . curl_error($ch); }
        curl_close($ch);
        return response()->json($result);
    }
}
