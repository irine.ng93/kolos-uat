<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Category;
use App\User;
use App\Service;
use App\Favorite;
use App\UserMerchant;
use App\Review;
use App\ProRequest;

use Auth;
use Validator;

class MerchantUccul extends Controller
{
    public function __construct()
    {
    	//$this->middleware('jwt.auth');
    }

    public function postRequest(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'company' => 'required|max:100',
            'email' => 'email|required|max:100|unique:pro_request,email'
        ]);

        if($valid->fails()){
            return response()->json([
               'status' => 203,
               'msg_status' => $valid->errors()->first(),
            ]);

        } else {
            $email = $request->input('email');
            $company = $request->input('company');
            $categoryName = $request->input('category');

            $user = User::where('email', $email)->first();
            if($user){
                $user->status = 1;
                $user->save();

                $category = Category::where('name',$categoryName)->first();
                $categoryID = $category->id;
                $userID = $user->id;

                $checkMerchant = UserMerchant::where('user_id',$userID)->first();
                if($checkMerchant == null){
                    $merchant = new UserMerchant;
                    $merchant->company = $company;
                    $merchant->category_id = $categoryID;
                    $merchant->user_id = $userID;

                    if($merchant->save())
                    {
                        return response()->json([
                           'status' => 200,
                           'msg_status'  => 'Request has been sent',
                        ]);
                    }
                    else
                    {
                        return response()->json([
                           'status' => 201,
                           'msg_status'  => 'Failed',
                        ]);
                    }
                }
                else{
                    return response()->json([
                       'status' => 201,
                       'msg_status'  => 'Failed',
                    ]);
                }
            }
            else{
                return response()->json([
                   'status' => 203,
                   'msg_status' => 'User not found',
                ]);
            }
        }
    }


    //  get all merchants
    public function getAll()
    {
    	$merchants = UserMerchant::where('active', 1)->get();

        if (Auth::check()) {
            $merchants = UserMerchant::where('active', 1)->where('user_id', '<>', Auth::id())->get();
        }

        if ($merchants->isEmpty()) {
            return response()->json(array(
                'status' => 201,
                'merchants' => 'No merchant found.',
            ));
        } else {
            return response()->json(array(
                'status' => 200,
                'merchants' => $merchants,
            ));
        }
    }

    // merchant detail
    public function getDetail(Request $request)
    {
    	$id = $request->input('id');
        $merchant = UserMerchant::where('active', 1)
            ->where('user_id', $id)->first();
        $services = Service::where('user_id', $id)->get();
        $reviews = Review::select('reviews.*','users.avatar','users.name','feedbacks.comment as merchant_feedback')
            ->where('reviews.merchant_id', $id)
            ->leftJoin('feedbacks','feedbacks.order_id','=','reviews.order_id')
            ->leftJoin('users','users.id','=','reviews.customer_id')
            ->orderBy('created_at', 'DESC')
            ->get();
        //$number_votes = Review::where('merchant_id', $id)->get()->count();
        //$user = User::find($request->input('id'));

        if (!$merchant)
        {
            return response()->json([
                'status' => 401,
                'error' => 'He is not a merchant',
            ]);
        }

        $merchant['total_reviews'] = $reviews->count();

        if($reviews->isEmpty())
        {
            $reviews = '[]';

        }

        if($services->isEmpty())
        {
            $services = '[]';
        }

        if ($request->input('user_id') != null)
        {
            $favorited = Favorite::where('user_id', $request->input('user_id'))
                ->where('merchant_id', $id)->first();

            if ($favorited) {
                $isFav = true;
            } else {
                $isFav = false;
            }

            if (!$merchant) {
                return response()->json(array(
                    'status' => 201,
                    'merchant' => 'No merchant found.',
                ));
            } else {
                return response()->json(array(
                    'status' => 200,
                    'favorite' => $isFav,
                    'merchant' => $merchant,
                    'services' => $services,
                    'reviews' => $reviews,
                ));
            }
        }
        else
        {
            if (!$merchant) {
                return response()->json(array(
                    'status' => 201,
                    'merchant' => 'No merchant found.',
                ));
            } else {
                return response()->json(array(
                    'status' => 200,
                    'merchant' => $merchant,
                    'services' => $services,
                    'reviews' => $reviews,
                ));
            }
        }

    }

    // get top merchants
    public function getFeatured(Request $request)
    {
    	$merchants = UserMerchant::where('active', 1)
    		->where('featured', 1)
    		->get();
        if ($request->has('authId')) {
            $merchants = UserMerchant::where('user_id', '<>', $request->input('authId'))
            ->where(function ($query) {
                $query->where('active', '=', 1)
                      ->orWhere('featured', '=', 1);
            })
            ->get();
        }
        if ($merchants->isEmpty()) {
            return response()->json(array(
                'status' => 201,
                'merchants' => 'No merchant found.',
            ));
        } else {
            return response()->json(array(
                'status' => 200,
                'merchants' => $merchants,
            ));
        }

    }


    // get list of favorites merchants
    public function getFavorites(Request $request)
    {
        $id = Auth::user()->id;
        $favorites = Favorite::select('favorite_user.id', 'favorite_user.user_id as userid', 'favorite_user.merchant_id', 'user_merchant.*')
            ->where('favorite_user.user_id', $id)
            ->where('user_merchant.active', 1)
            //->leftJoin('users', 'users.id', '=', 'favorite_user.merchant_id')
            ->leftJoin('user_merchant', 'user_merchant.user_id', '=', 'favorite_user.merchant_id')
            ->get();
        if ($favorites->isEmpty()) {
            return response()->json(array(
                'status' => 201,
                'favorites' => 'No favorites merchant yet.',
            ));
        } else {
            return response()->json(array(
                'status' => 200,
                'favorites' => $favorites,
            ));
        }

    }

    //search in mobile app
    public function getSearchMobile(Request $request){
        $keyword = $request->input('keyword');
        $merchants = UserMerchant::select('user_id','company')
                    ->where('active','=',1)
                    ->where(function ($query) use ($keyword) {
                        $query->where('company', 'like', '%' . $keyword . '%')
                              ->orWhere('description','like','%' . $keyword . '%');
                    })
                    ->offset(0)
                    ->limit(20)
                    ->get();
        $services = Service::select('user_merchant.user_id','user_merchant.company','services.name')
                    ->rightJoin('user_merchant', 'user_merchant.user_id', '=', 'services.user_id')
                    ->where('services.name', 'like', '%' . $keyword . '%')
                    ->where('user_merchant.active','=',1)
                    ->offset(0)
                    ->limit(20)
                    ->get();

        return response()->json(array(
            'status' => 200,
            'error' => false,
            'data_merchant' => $merchants,
            'data_service' => $services,
        ));
    }
    //end search in mobile app

    // the magic search
    public function getSearch(Request $request)
    {
        $query = Service::query();

        if ($request->has('keywords'))
        {
            $key = $request->input('keywords');
            $query->where('name','like',"%$key%");
            $query->orWhere('description','like',"%$key%");
        }

        $results = $query->orderBy('name', 'ASC')->get();

        return response()->json([
            'status'    => '200',
            'data'      => $results
        ]);
    }

    public function getSearchNew(Request $request)
    {
        $query = UserMerchant::query();

        if ($request->has('keywords'))
        {
            $key = $request->get('keywords');
            $query->where('company','like',"%$key%");
            $query->orWhere('description','like',"%$key%");
        }

        $results = $query->orderBy('company', 'ASC')->get();

        return response()->json($results);
    }


    public function addFavorite(Request $request)
    {
        // params ['token','merchant_id'];

        $user_id = Auth::user()->id;
        $merchant_id = $request->input('merchant_id');

        $favorited = Favorite::where('user_id', $user_id)->where('merchant_id', $merchant_id)->first();

        if ($favorited) {
            $favorite = $favorited;
        } else {
            $favorite = new Favorite;
        }
        $favorite->user_id = $user_id;
        $favorite->merchant_id = $merchant_id;
        if ($favorite->save()) {
            return response()->json(array(
                'status' => 200,
                'data' => 'Successfully added to favorite',
            ));
        } else {
            return response()->json(array(
                'status' => 201,
                'data' => 'Cannot add to favorite',
            ));
        }
    }


    public function removeFavorite(Request $request)
    {
        // params ['token','merchant_id'];

        $user_id = Auth::user()->id;
        $merchant_id = $request->input('merchant_id');

        $user = Favorite::where('user_id', $user_id)
            ->where('merchant_id', $merchant_id)->first();
        if ($user->delete()) {
            return response()->json(array(
                'status' => 200,
                'data' => 'Successfully remove favorite',
            ));
        } else {
            return response()->json(array(
                'status' => 201,
                'data' => 'Failed to remove favorite',
            ));
        }
    }
}
