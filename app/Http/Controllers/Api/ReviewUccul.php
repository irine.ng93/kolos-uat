<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Review;
use App\Feedback;
use App\UserMerchant;
use App\Order;
use App\User;
use App\Alert;

use Validator;
use Auth;

class ReviewUccul extends Controller
{

    public function remainingReviews()
    {

        $somethingToReview = Order::where('user_id', Auth::id())
            ->where('status', 2)
            //->doesnthave('review')
            ->with('merchant')
            ->with('review')
            ->orderBy('id', 'DESC')->get();

        return response()->json(array(
            'status' => 200,
            'orders' => $somethingToReview,
        ));
    }

    public function pleaseReview()
    {
        $somethingToReview = Order::where('user_id', Auth::id())
            ->where('status', 2)
            ->doesnthave('review')
            ->with('merchant')
            // ->with('review')
            ->orderBy('id', 'ASC')->get();

        return response()->json(array(
            'status' => 200,
            'orders' => $somethingToReview,
        ));
    }

    public function getReviews(Request $request)
    {
        $reviews = Review::where('merchant_id', $request->input('merchant_id'))->get();
        $feedback = Feedback::where('merchant_id', $request->input('merchant_id'))->first();
        if (!$reviews->isEmpty()) {
            return response()->json(array(
                'status' => 200,
                'review' => $reviews,
                'feedback' => $feedback,
            ));
        } else {
            return response()->json(array(
                'status' => 201,
                'review' => 'No reviews yet',
            ));
        }
    }

    public function getLastreview(Request $request)
    {
        $lastOrder = Order::where('user_id', Auth::user()->id)
            ->where('status', 2) // check if status is complete
            ->orderBy('id', 'DESC')->first();
        $review = Review::where('order_id', $lastOrder['id'])->first();

        if ($review == null)
        {
            $merchant = UserMerchant::where('user_id', $lastOrder['merchant_id'])->first();
            $lastOrder['merchant_name'] = $merchant['company'];
            return response()->json([
                'status'                => 200,
                'review_data'           => null,
                'last_complete_order'   => $lastOrder,
            ]);
        }
        else
        {
            $merchant = UserMerchant::where('user_id', $review['merchant_id'])->first();
            $review['merchant_name'] = $merchant['company'];
            return response()->json([
               'status'     => 201,
                'review_data'      => $review,
            ]);
        }
    }

    public function getReviewByOrderID(Request $request)
    {
        $order_id = $request->input('order_id');
        $review = Review::where('order_id', $order_id)->first();

        if ($review == null)
        {
            return response()->json([
                'status'        => 201,
                'has_review'    => 0
            ]);
        }
        else
        {
            return response()->json([
                'status'        => 200,
                'has_review'    => 1,
            ]);
        }
    }

    public function postReview(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id'    => 'required|integer',
            'merchant_id'   => 'required|integer',
            'comment'       => 'max:255',
            'rating'        => 'required',
        ]);

        if($valid->fails())
        {
            return response()->json([
               'status' => 400,
               'errors' => $valid->errors(),
            ]);
        }
        else {
            //$merchant_id = $request->input('merchant_id');
            $user_id = Auth::user()->id;
            $review = new Review;
            $review->order_id = $request->input('id');
            $review->customer_id = $user_id;
            $review->merchant_id = $request->input('merchant_id');
            $review->comment = $request->input('comment');
            $review->rating = $request->input('rating');

            if ($review->save()) {

                $merchant = UserMerchant::where('user_id', $request->input('merchant_id'))->first();
                $user = User::find($merchant->user_id);

                // Mail::send('emails.review', ['user' => $merchant], function ($m) use ($user) {
                //    $m->to($user->email, $user->name)->subject('Hi, '. $user->name .'. You just got new review from '. Auth::user()->name);
                // });

                $message = 'Hi '. $user->name .', You just got new review from '. Auth::user()->name;
                $this->push($user->id, $message, 1);
                
                $number_votes = Review::where('merchant_id', $request->input('merchant_id'))->get()->count();
                $points = Review::select('rating')->where('merchant_id', $request->input('merchant_id'))->get();
                if ($points->isEmpty()) {
                    $merchant->rating = $request->input('rating');
                    $merchant->save();

                    // save alert notification
                    $user = User::find($user_id);
                    $alert = new Alert;
                    $alert->user_id = $merchant_id;
                    $alert->message = 'You get new review from '. $user->name .'. You can check it in your profile.';
                    $alert->message_id = 'Anda mendapat ulasan baru dari '. $user->name .'. Ulasan bisa dilihat di profil Anda.';
                    $alert->icon = '1';
                    $alert->save();

                    $this->push($request->input('merchant_id'), $alert->message, 1);

                    return response()->json(array(
                        'status' => 200,
                        'data' => 'Review successfully saved',
                    ));

                } else {
                    $points_count = 0;
                    foreach ($points as $point) {
                        $points_count += $point->rating;
                    }
                    $total_points = $points_count;
                    $merchant->rating = round($total_points / $number_votes, 1);
                    $merchant->total_reviews = $number_votes + 1;
                    $merchant->save();

                    // save alert notification
                    $user = User::find($user_id);
                    $alert = new Alert;
                    $alert->user_id = $merchant->id;
                    $alert->message = 'You get new review from '. $user->name .'. You can check it and reply in your profile.';
                    $alert->message_id = 'Anda mendapat ulasan baru dari '. $user->name .'. Ulasan bisa dilihat di profil Anda.';
                    $alert->icon = '1';
                    $alert->save();

                    return response()->json(array(
                        'status' => 200,
                        'data' => 'Review successfully saved',
                    ));
                }

            } else {
                return response()->json(array(
                    'status' => 201,
                    'data' => 'Review cannot be saved',
                ));
            }
        }

    }

    // post feedback by merchant
    public function postFeedback(Request $request)
    {
        if (Auth::user()->status != 1)
        {
            return response()->json([
                'error' => 1,
                'data'  => 'Youre not a merchant',
            ]);
        }

        $ID = Auth::user()->id;

        $feedback = new Feedback;
        $feedback->order_id = $request->input('id');
        $feedback->merchant_id = $ID;
        $feedback->comment = $request->input('comment');

        if ($feedback->save()) {

            // save alert notification
            $user = UserMerchant::find($ID);
            $alert = new Alert;
            $alert->user_id = $ID;
            $alert->message = 'You get new feedback from '. ($user->company == '' ? 'The Merchant' : $user->company) .'. You can check it from order page.';
            $alert->icon = '1';
            $alert->save();

            return response()->json(array(
                'status' => 200,
                'data' => 'Feedback successfully saved',
            ));
        } else {
            return response()->json(array(
                'status' => 201,
                'data' => 'Feedback cannot be saved',
            ));
        }
    }


    // push notifications
    public function push($user_id, $message, $eventId)
    {
        $apiKey = "AAAAS7PV8yU:APA91bGMYq7LJ1DpY9XkDUKaxjjWHk2S3SsW-Aw5Xk8FyleBwMgR6EdXAMq8wa6TtYIhtoSID3FehuA6MH0Foo_qBqes1SDaFzLs7jYTBgTpTXs29iryThtJ0eFrbrH4rCx4-qRbhXDw";
        $user = User::find($user_id);
        $registrationIDs = array($user->gcm_id);
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
                'registration_ids'  => $registrationIDs,
                'data'              => array("message" => $message, "eventId" => $eventId),
                );
        $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
                );
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $result = curl_exec($ch);
        if(curl_errno($ch)){ echo 'Curl error: ' . curl_error($ch); }
        curl_close($ch);
        return response()->json($result);
    }
}
