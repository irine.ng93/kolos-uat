<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\User;
use App\UserMerchant;
use App\RequestService;
use App\Review;

class CategoryUccul extends Controller
{
    public function getAll()
    {
    	$categories = Category::all();
    	return response()->json(array(
    		'data' => $categories,
    		'status' => 200,
    	));
    }

    public function getTop()
    {
    	$categories = Category::getTop();
    	return response()->json(array(
    		'data' => $categories,
    		'status' => 200,
    	));
    }

    public function getDetail(Request $request)
    {
        $id = $request->input('id');
        $users = UserMerchant::where('active', 1)
            ->where('category_id', $id)
            ->get();
        $countUser = 0;
        foreach ($users as $user) {
            $reviewAmount = Review::where('merchant_id', $user->user_id)->count();
            $users[$countUser]['total_reviews'] = $reviewAmount;
            $countUser++;
        }
        if ($request->has('authId')) {
            $users = UserMerchant::where('category_id', '=', $id)
                ->where('user_id', '<>', $request->input('authId'))
                ->where(function ($query) {
                    $query->where('active', 1);
            })
            ->get();
            $countUser = 0;
            foreach ($users as $user) {
                $reviewAmount = Review::where('merchant_id', $user->user_id)->count();
                $users[$countUser]['total_reviews'] = $reviewAmount;
                $countUser++;
            }
        }
        if($users->isEmpty())
        {
            return response()->json(array(
                'status' => 201,
                'users' => 'No merchant found.',
            ));
        }
        else
        {
            return response()->json(array(
                'status' => 200,
                'users' => $users,
            ));
        }
    }

    public function requestService(Request $request){
        $service = $request->input('service');
        $userID = $request->input('user_id');

        $query = new RequestService;
        $query->service_name = $service;
        $query->user_id = $userID;
        $query->created_at = date('Y-m-d H:i:s');

        if($query->save(['timestamps' => false])){
            $responseStatus = 200;
        }
        else{
            $responseStatus = 201;
        }

        return response()->json(array(
            'status' => $responseStatus
        ));
    }
}
