<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Validator;

use App\Order;
use App\User;
use App\Service;
use App\Category;
use App\UserMerchant;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $merchants = UserMerchant::where('lat','<>','')->where('lon','<>','')->get();
      $merchantNum = UserMerchant::where('lat','<>','')->where('lon','<>','')->count();
      return view('admin.dashboard.view')
          ->with('merchants', $merchants)
          ->with('merchantNum', $merchantNum);
    }

    // view user profile
    public function getViewProfile($id)
    {
        $user = User::find($id);
        $orders = Order::where('user_id', $id)->where('merchant_id', Auth::id())->paginate('10');
        return view('admin.user.profile')
            ->with('orders', $orders)
            ->with('user', $user);
    }
}
