@extends('layouts.app')
@section('content')
<?php
    if(Input::has('search_name')){
        $searchName = Input::get('search_name');
    }
    else{
        $searchName = "";
    }
    if(Input::has('search_email')){
        $searchEmail = Input::get('search_email');
    }
    else{
        $searchEmail = "";
    }
    if(Input::has('search_phone')){
        $searchPhone = Input::get('search_phone');
    }
    else{
        $searchPhone = "";
    }
    if(Input::has('search_active')){
        $searchActive = Input::get('search_active');
    }
    else{
        $searchActive = "";
    }
    if(Input::has('sortby')){
        $sortBy = Input::get('sortby');
    }
    else{
        $sortBy = "";
    }
    if(Input::has('sorttype')){
        $sortType = Input::get('sorttype');
    }
    else{
        $sortType = "DESC";
    }
?>

<div class="col-md-12">
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-11">
                    <h4 class="title">Users Data</h4>
                </div>
                <div class="col-md-1">
                    <a href="{{ url('home/user/customer') }}" class="btn btn-success" rel="tooltip" data-original-title="Reset Filter">
                        <span class="glyphicon glyphicon-repeat right-element"></span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 normal-text">
                    Showing Data: {{ $users->count() }} of {{ $users->total() }}
                </div>
            </div>
        </div>
        <div class="content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center"><a href="" id="c_sort_id">ID <i class="fa fa-sort-down"></i></a></th>
                        <th><a href="" id="c_sort_name">Name <i class="fa fa-sort-down"></i></a></th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th><a href="" id="c_sort_since">Since <i class="fa fa-sort-down"></i></a></th>
                        <th><a href="" id="c_sort_login">Last Login <i class="fa fa-sort-down"></i></a></th>
                        <th style="text-align:center;">
                            Active
                        </th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tr>
                    <form method="get" id="filter_form">
                        <input type="hidden" class="form-control" id="input_sortby" name="sortby" value="{{ Request::get('sortby') }}" />
                        <input type="hidden" class="form-control" id="input_sorttype" name="sorttype" value="{{ Request::get('sorttype') }}" />
                        <td></td>
                        <td>
                            <input type="text" class="form-control" id="search_name" name="search_name" placeholder="Search name" value="{{ Request::get('search_name') }}" />
                        </td>
                        <td>
                            <input type="text" class="form-control" id="search_email" name="search_email" placeholder="Search email" value="{{ Request::get('search_email') }}" />
                        </td>
                        <td>
                            <input type="text" class="form-control" id="search_phone" name="search_phone" placeholder="Search phone" value="{{ Request::get('search_phone') }}" />
                        </td>
                        <td>
                            <input type="text" class="form-control" id="search_registerdate" name="search_registerdate" value="" />
                        </td>
                        <td></td>
                        <td>
                            <select class="form-control" name="search_active" id="search_active">
                                <option>-</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                                <option value="all">All</option>
                            </select>
                        </td>
                        <td></td>
                        <input type="submit" style="display: none;">
                    </form>
                </tr>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td class="text-center">{{ $user->id }}</td>
                        <td><a href="{{ url('home/view/profile/'. $user->id) }}">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>
                            {{ date('d F Y', strtotime($user->created_at)) }}
                        </td>
                        <td>
                            {{ $user->last_login == null? '' : date('d F Y', strtotime($user->last_login)) }}
                        </td>
                        <td style="text-align: center;">
                            @if($user->active == 0)
                            <form action="{{ url('home/user/idup') }}" method="post" style="display: inline">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{ $user->id }}" name="id">
                                <button type="submit" name="button" rel="tooltip" data-original-title="Click to Activate" class="btn btn-default btn-round btn-xs"><i class="fa fa-circle text-default" style="text-align: left"></i></button>
                            </form>
                            @else
                            <form action="{{ url('home/user/mati') }}" method="post" style="display: inline">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{ $user->id }}" name="id">
                                <button type="submit" name="button" rel="tooltip" data-original-title="Click to Deactivate" class="btn btn-default btn-round btn-xs"><i class="fa fa-circle text-success" style="text-align: right"></i></button>
                            </form>
                            @endif
                        </td>
                        <td class="td-actions text-right">
                            <a href="#" rel="tooltip" title="" class="btn btn-info btn-simple btn-xs" data-original-title="View Profile">
                                <i class="fa fa-user"></i>
                            </a>
                            <a href="{{ url('home/user/edit/'. $user->id) }}" rel="tooltip" title="" class="btn btn-success btn-simple btn-xs" data-original-title="Edit User">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="footer">
            <div class="legend">
                <i class="fa fa-circle text-default"></i> Inactive
                <i class="fa fa-circle text-success"></i> Active
                <i class="fa fa-circle text-danger"></i> Top
            </div>
        </div>

        <div class="text-center">
            {!! $users->appends(array("search_name" => $searchName, 
                                    "search_email" => $searchEmail, 
                                    "search_phone" => $searchPhone,
                                    "search_active" => $searchActive,
                                    "sortby" => $sortBy, 
                                    "sorttype" => $sortType))->links() !!}
        </div>
    </div>
</div>

@endsection

