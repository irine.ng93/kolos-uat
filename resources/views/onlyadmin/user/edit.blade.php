@extends('layouts.app')
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Profile</h4>
                    </div>
                    <div class="content">
                        <form action="{{ url('home/user/update') }}" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label>Username <star>*</star></label>
                                        <input type="text" name="username" class="form-control" placeholder="Username" value="{{ $user->username }}">
                                        @if ($errors->has('username'))
                                            <label class="error">{{ $errors->first('username') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Full Name <star>*</star></label>
                                        <input type="text" name="name" class="form-control" placeholder="Full Name" value="{{ $user->name }}">
                                        @if ($errors->has('name'))
                                            <label class="error">{{ $errors->first('name') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label>Phone Number <star>*</star></label>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ $user->phone }}" data-inputmask="'mask' : '+62 999-9999-9999'" maxlength="18">
                                        @if ($errors->has('phone'))
                                            <label class="error">{{ $errors->first('phone') }}</label>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="exampleInputEmail1">Email address <star>*</star></label>
                                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}">
                                        @if ($errors->has('email'))
                                            <label class="error">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('home_address') ? ' has-error' : '' }}">
                                        <label>Home Address <star>*</star></label>
                                        <input id="home-address-field" type="text" name="home_address" value="{{ $user->home_address }}" class="form-control">
                                        @if ($errors->has('home_address'))
                                            <label class="error">{{ $errors->first('home_address') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="home-address-map" style="width:100%; height: 250px;"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('work_address') ? ' has-error' : '' }}">
                                        <label>Work Address <star>*</star></label>
                                        <input id="work-address-field" type="text" name="work_address" value="{{ $user->work_address }}" class="form-control">
                                        @if ($errors->has('work_address'))
                                            <label class="error">{{ $errors->first('work_address') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="work-address-map" style="width:100%; height: 250px;"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('other_address') ? ' has-error' : '' }}">
                                        <label>Other Address</label>
                                        <input type="text" name="other_address" value="{{ $user->other_address }}" class="form-control">
                                        @if ($errors->has('other_address'))
                                            <label class="error">{{ $errors->first('other_address') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">New Password</label>
                                        <input type="password" name="password" class="form-control">
                                        @if ($errors->has('password'))
                                            <label class="error">{{ $errors->first('password') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password_confirmation">Password Again</label>
                                        <input type="password" name="password_confirmation" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <button type="submit" class="btn btn-warning btn-fill pull-right">Update Profile</button>
                            <div class="clearfix"></div>
                        </form>
                        <form id="upload_avatar" action="{{ url('home/user/uploadimageuser') }}" method="post" enctype="multipart/form-data" style="display: none">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                            <input id="avatar_field" type="file" class="form-control" name="avatar" accept="image/jpeg,image/png">
                            <input type="hidden" name="id" value="{{ $user->id }}"></input>
                            <input type="hidden" name="name" value="{{ $user->name }}"></input>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="content">
                        <div class="author">
                             <a href="#">
                                 <img id="profile-image" class="avatar border-gray" src="{{ ($user->avatar != null ? $user->avatar : url('lib/img/default_avatar.png')) }}" alt="Avatar Image"/>
                                 <img class="avatar border-gray loading-image" src="https://media.giphy.com/media/JBeu9q9LC1Kve/giphy.gif" style="display: none">
                                  <h4 class="title">{{ $user->name }}<br />
                                     <small>{{ $user->username }}</small>
                                  </h4>
                            </a>
                        </div>
                        <p class="description text-center">
                            {{ $user->location }}
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="{{ url('dist/js/input_mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ url('dist/js/jquery.form.js') }}"></script>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyB0BXhT7bYjEqTiyjVIKswpmdMVm9XbV2k'></script>
<script type="text/javascript" src="{{ url('lib/js/locationpicker.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function () {
      $(":input").inputmask();

      $("#profile-image").click(function() {
          $("#avatar_field").click();
      });

      // geo location maps
      $('#home-address-map').locationpicker(
          {
              // default location
              location:
              {
                  latitude: -6.251176399999999,
                  longitude: 106.79995910000002,
              },
            radius: 100,
              zoom: 16,
              scrollwheel: false,
              inputBinding:
              {
                  latitudeInput: $('#glat'),
                  longitudeInput: $('#glon'),
                  radiusInput: $('#gradius'),
                  locationNameInput: $('#home-address-field'),
                  radiusInput: $('#gradius'),
              },
              enableAutocomplete: true,

          }
      ); // pick location from google map


      $('#work-address-map').locationpicker(
          {
              // default location
              location:
              {
                  latitude: -6.251176399999999,
                  longitude: 106.79995910000002,
              },
            radius: 100,
              zoom: 16,
              scrollwheel: false,
              inputBinding:
              {
                  latitudeInput: $('#glat'),
                  longitudeInput: $('#glon'),
                  radiusInput: $('#gradius'),
                  locationNameInput: $('#work-address-field'),
                  radiusInput: $('#gradius'),
              },
              enableAutocomplete: true,

          }
      ); // pick location from google map

      // ajax to upload avatar
      $('#avatar_field').change(function() {
          var status = $('#status');

          $('#upload_avatar').ajaxForm({
              beforeSend: function() {
                  status.empty();
              },
              uploadProgress: function(event, position, total, percentComplete) {
                  $('#profile-image').css('display','none');
                  $('.loading-image').css('display','inherit');
                  //$('.placeholder-image').css('opacity','0.5');
              },
              complete: function(xhr) {
                  var x = xhr.responseText.replace(/\"|\\/g, "");
                  console.log(x);
                  $('#profile-image').css('display','inherit');
                  $('.loading-image').css('display','none');
                  $("#profile-image").attr("src", x);
                  //$('.placeholder-image').css('opacity','1');
              }
          });

          $('#upload_avatar').submit();
      });
  });
</script>
@stop
