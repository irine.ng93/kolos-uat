@extends('layouts.app')

@section('content')
<div class="">
  <div class="clearfix"></div>

  <div class="row">

    <div class="col-md-12">
      <div class="card">
        <div class="header">
            <h2>Services Data <a href="{{ url('home/service/create') }}" class="btn btn-primary btn-sm">Create new</a></h2>
            <div class="clearfix"></div>
        </div>

        <div class="content">
        @if($services->isEmpty())
            <p>No service yet.</p>
        @else
            <table class="table">
                <thead>
                    <tr>
                       <th>ID</th>
                        <th style="width: 40%">Name</th>
                        <th style="width: 20%">IDR</th>
                        <th style="width: 30">Owner</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                    <tr class="tr-content">
                       <td class="td-content">{{ $service->id }}</td>
                        <td class="td-content">{{ $service->name }}</td>
                        <td class="td-content">{{ number_format($service->price, 0) }}</td>
                        <td class="td-content">{{ $service->user->merchant->company }}</td>
                        <td>
                            <a href="{{ url('home/service/edit/'. $service->id) }}" class="btn btn-primary btn-xs">Edit</a>
                                <form action="{{ url('home/service/delete') }}" method="post" style="display: inline">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{ $service->id }}" name="id">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-xs">
                                </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
            <div class="pull-left">
                {!! $services->render() !!}
            </div>
        @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
